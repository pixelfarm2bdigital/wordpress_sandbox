<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache



/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

require_once( 'wp-config.local.php' );

if(!defined("WP_PROTO")){
	define( 'WP_PROTO', "http");
}

 //Added by WP-Cache Manager

define( 'WP_SITEURL', WP_PROTO.'://' . $_SERVER['SERVER_NAME'] . '/wp' );
define( 'WP_HOME', WP_PROTO.'://' . $_SERVER['SERVER_NAME'] );

define( 'WP_CONTENT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content' );
define( 'WP_CONTENT_URL', WP_PROTO.'://' . $_SERVER['SERVER_NAME'] . '/wp-content' );

define( 'WP_PLUGIN_DIR', $_SERVER['DOCUMENT_ROOT'] . '/wp-content/plugins' );
define( 'WP_PLUGIN_URL', WP_HOME. '/wp-content/plugins' );



define( 'EMPTY_TRASH_DAYS', 365 );
define( 'WP_POST_REVISIONS', 250 );


if(!defined("VERSION")){
	define("VERSION",time());
}


define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7cYxs#L>nkF+#|CJYCWBlwqrQ!Vd|&OLUATDgC)l:Crs8hhoV?$)fUdmr[F8H()b');
define('SECURE_AUTH_KEY',  'gwik-^aNP|O^e+oW@15IH&+-0Ul`QxRJ$j;a^M-Vtf)S=z0tye*)RG4#L6y}+i/8');
define('LOGGED_IN_KEY',    '#E51i$gZ~3U`h&1}[5RH/l}?+@zAP$RwN:}$wbTTDmdI5juh|u7U_M27H|k<w4-b');
define('NONCE_KEY',        '!+|OX:1d|4nd$jD*,<q<Vt2p}H(xmO/f`DbW0S-2ae49P4>wm&Suz4Uf!;p??NS9');
define('AUTH_SALT',        'E4(fSn5+r2m50X`2}iGsV-ktyF#ePU)xQ6W/]Zeqt`&@-AE:^R,Y2|*Bb|rE-b<l');
define('SECURE_AUTH_SALT', '-!2<]6Kn/_~Td<nx<f,T;YyT*HKrp*yf-Cp*DPC,tNu5@hGAM0!f4 ai#}BIt>Z/');
define('LOGGED_IN_SALT',   '[cY)(hy-[tH&fl{l-1s>S/v~!pXyHF,fG?@h~y?zj/2.MJT5-5KE?YhpAuz; ]OR');
define('NONCE_SALT',       'C+c7(43TU|[m9v9,Os<>h,|sli/[GOvunqKE!;L(p.Olkg8CH56+ByV;4R,+ S:$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'pxlfrm_wp_';

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

/**
 * Disable search engines when not on production server
 */
if ( !WP_INDEX ) { add_filter( 'pre_option_blog_public', '__return_zero' ); } // Privacy, robots no-follow metatag

/**
 * Fix chmod
 * file acces read/write for ftp
 */
//if( is_admin() ) { add_filter( 'filesystem_method', create_function('$a', 'return "direct";' ) ); define( 'FS_CHMOD_DIR', 0751 ); }