<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/custom-post-types.php',     // Theme setup
  'lib/acf-options.php',     // ACF options
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/hm.php',         // hm
  'lib/shortcodes.php', // shortcode
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

unset($file, $filepath);

function html5_search_form( $form ) { 
     $form = '<section class="search"><form role="search" method="get" id="search-form" action="' . home_url( '/' ) . '" >
    <label class="screen-reader-text" for="s">' . __('',  'domain') . '</label>
     <input type="search" class="search-field" value="' . get_search_query() . '" name="s" id="s" placeholder="Zoek op productnaam, diagnose of situatie" />
     <div class="submit__wrapper"><input type="submit" class="search-submit" id="searchsubmit" value="'. esc_attr__('', 'domain') .'" /></div>
     </form></section>';
     return $form;
}

 add_filter( 'get_search_form', 'html5_search_form' );


//add_action("admin_head","load_custom_wp_tiny_mce");
function load_custom_wp_tiny_mce() {

$editor_id = 'form_description_input';

wp_editor( $content, $editor_id );


}

/* START TWITTER / FUNCTIONS */
require get_template_directory() . '/lib/twitteroauth/autoload.php';

function startsWith($haystack, $needle)
{
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function endsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }

    return (substr($haystack, -$length) === $needle);
}


function enqueue_gravityform($formid){
  if(!isset($GLOBALS['gravityforms'])){
    $GLOBALS['gravityforms'] = array();
  }
  $GLOBALS['gravityforms'][] =  $formid;
}



function get_url_for_language( $original_url, $language = ICL_LANGUAGE_CODE )
{
    $post_id = url_to_postid( $original_url );
    $lang_post_id = icl_object_id( $post_id , 'page', true, $language );
     
    $url = "";
    if($lang_post_id != 0) {
        $url = get_permalink( $lang_post_id );
    }else {
        // No page found, it's most likely the homepage
        global $sitepress;
        $url = $sitepress->language_url( $language );
    }
     
    return $url;
}