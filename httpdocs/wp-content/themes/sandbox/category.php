<?php
/**
* A Simple Category Template
*/
$_SESSION['global_cat'] = 'neutraal';

hm_get_template_part("parts/elements/menu", array('type'=>$_SESSION['global_cat']));

hm_get_template_part("parts/elements/headers", array("type"=>"page"));

$max = 6;

?>

<div class="container page__innovatie">
	<?php 
		$cat = get_the_category(); 
	?>

	<h1><?php echo $cat[0]->name; ?></h1>

	<div class="innovatie__wrapper">
		<div class="innovatie__filter">
			<span>Filter innovatie:</span>
			<div class="pill__wrapper">
				<a href="/category/slimme-veilige-zorg" class="pill pill--professional">Zakelijk</a>
				<a href="/category/veilig-vitaal-thuis" class="pill pill--consument">Consument</a>
				<a href="/innovatie/" class="pill">Toon alleen nieuws</a>
			</div>
		</div>

		<div class="innovatie__news">
			<?php

				$i = 0;

				if ( have_posts() ) :
					while ( have_posts() ) : the_post();

						if($i % 3 == 0){
							echo "</div><div class='news__row'>";
						}

						$id = get_the_ID();
						$i ++;

						hm_get_template_part("parts/elements/news_single", array("post"=> $id, "addClass"=> 'post'.$i)); 

					endwhile;
				else:
					echo 'Er zijn geen berichten in deze categorie';

				endif
			
			?>
		</div>

		<?php
		/*
			if($i >= $max){
				?>
					<div class="button__container">
						<a href="#" class="button button--line">Meer laden</a>
					</div>
				<?php
			}
		*/
		?>
		
	</div>
</div>
