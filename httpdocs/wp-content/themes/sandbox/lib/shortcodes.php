<?php

function calltoaction_shortcode( $atts ) {
	$a = shortcode_atts( array(
		'id' => '0',
	), $atts );


	$bar = hm_get_template_part('parts/footerbars/footerbar_call_to_action_bar',
		array(
			"bar"=>array(
				$atts['id']
			),
			"return"=>true
		)
	);
	
	return $bar;
}

add_shortcode( 'calltoaction', 'calltoaction_shortcode' );
add_shortcode( 'call2action', 'calltoaction_shortcode' );