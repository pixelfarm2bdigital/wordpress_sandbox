<?php

namespace Roots\Sage\Setup;

use Roots\Sage\Assets;


function getGoogleFonts(){
  return "https://fonts.googleapis.com/css?family=Lato:400,700,900,400i,700i,900i|Open+Sans:400,700|Ubuntu+Mono:700";
}

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');


  //
  DEFINE('ASSETS_URL', get_template_directory_uri().'/assets/'); 

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'sage')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  add_editor_style(getGoogleFonts());
  add_editor_style(Assets\asset_path('styles/main.css'));

  add_image_size( 'overview_image', 208, 138, true );
  add_image_size( 'nieuws_overview', 259, 171, true );
  add_image_size( 'nieuws_featured', 380, 200, true );
  add_image_size( 'persoon', 259, 248, true );
  add_image_size( 'header', 615, 320, true );
  add_image_size( 'intro_image', 522, 348, false );
  add_image_size( 'quote_image', 354, 273, false );

}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Primary', 'sage'),
    'id'            => 'sidebar-primary',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);

  register_sidebar([
    'name'          => __('Footer', 'sage'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');



/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    !is_single( 'post' )
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
function assets() {

 

  wp_enqueue_style('sage/fonts', getGoogleFonts(), false, null);
  
  wp_enqueue_style('sage/css', Assets\asset_path('styles/main.css'), false, null);

  wp_enqueue_style('sage/fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, null);

  if (is_single() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }



  

  wp_enqueue_script('sage/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
      // Localize the script with new data
  $translation_array = array(
    'assets_url' => ASSETS_URL,
    //'ajax_url' => '10'
  );
  wp_localize_script( 'sage/js', 'sitevars', $translation_array );

}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);



// ADD NEW COLUMN
function training_modify_column_headers($defaults) {

  unset($defaults['date']);
  $defaults['training_date'] = 'Date';
  return $defaults;
}
 
// SHOW THE FEATURED IMAGE
function training_modify_column_content($column_name, $post_ID) {
  if ($column_name == 'training_date') {
    $date = get_field("date",$post_ID);
      if ($date) {
          echo date("Y-m-d",strtotime($date));
      }
  }
}

function my_relationship_result( $title, $post, $field, $post_id ) {
  
  // load a custom field from this $object and show it in the $result
  $footerbar_type = get_field_object('type', $post->ID);
  
  $value = get_field('type', $post->ID);
  // append to title
  if(!get_field('name', $post->ID) == 'type'){
    $title .= ' [ '.$footerbar_type['choices'][$value].' ]';
  }
  

  //print_r($value);
  
  // return
  return $title;  
}

// filter for every field
add_filter('acf/fields/relationship/result', __NAMESPACE__ . '\\my_relationship_result', 10, 4);


function add_acf_columns ( $columns ) {
   return array_merge ( $columns, array ( 
     'type' => __ ( 'Type' )
   ) );
 }
 add_filter ( 'manage_ctabar_posts_columns', __NAMESPACE__ . '\\add_acf_columns' );

/*
 * Add columns to exhibition post list
 */
function ctabar_custom_column ( $column, $post_id ) {
  $footerbar_type = get_field_object('type', $post_id);
  $value = get_field('type', $post_id);

   switch ( $column ) {
     case 'type':
       echo $footerbar_type['choices'][$value];
       break;
   }
 }
 add_action ( 'manage_ctabar_posts_custom_column', __NAMESPACE__ . '\\ctabar_custom_column', 10, 2 );


function training_modify_column_sortabe( $columns ) {
    $columns['training_date'] = 'training_date';
 
    //To make a column 'un-sortable' remove it from the array
    //unset($columns['date']);
 
    return $columns;
}


add_filter('manage_training_posts_columns',  __NAMESPACE__ . '\\training_modify_column_headers');
add_action('manage_training_posts_custom_column',  __NAMESPACE__ . '\\training_modify_column_content', 10, 2);

add_filter( 'manage_edit-training_sortable_columns', __NAMESPACE__ . '\\training_modify_column_sortabe' );

